# Tridion 8.5 GUI Extensions for Kaiser Permanente


Includes: 

* Validate Title Field (Validates title field for components on save or check in)


See the readme in the `ValidateTitleField` folder for more information.
