# ValidateTitleField GUI Extension for Tridion 8.5

This will block attempts to save or check-in a component in Tridion 8.5 if the component contains diacritics ( � � �)

# Installation

## Create Folders on the disk

1. On `D:\` create a folder called `CMS\GUIExtensions\ValidateTitleField`
2. Copy the `Configuration` and `Scripts` Folders from this project

## Create a Virtual Directory in IIS

1. Open IIS and expand the `SDL Web` site
2. Expand `WebUI/Editors`
3. Create a virtual directory that points to the physical location you created. Call it `ValidateTitleField`

## Edit the Tridion System Configuration

1. Open the file explorer and navigate to `SDL Web\web\WebUI\Configuration`
2. Open `System.config`
3. Add the following at the end of the `editors` node:

```
	<editor name="ValidateTitleField">
		<installpath>D:\CMS\GUIExtensions\ValidateTitleField\</installpath>
		<configuration>Configuration\ValidateTitleField.config</configuration>
		<vdir>ValidateTitleField</vdir>
	</editor>
```