﻿Type.registerNamespace("Extensions.ValidateTitleField");

Extensions.ValidateTitleFieldCommand = function ValidateTitleFieldCommand() {
    Type.enableInterface(this, "Extensions.ValidateTitleFieldCommand");
    this.addInterface("Tridion.Cme.Command", ["ValidateTitleFieldCommand"]);
}

/**
 * Regular Expression that covers a huge range of diacritics
 * @constant 
 */
Extensions.ValidateTitleFieldCommand.prototype._diacriticRegExp = new RegExp(/[À-ÖØ-öø-įĴ-őŔ-žǍ-ǰǴ-ǵǸ-țȞ-ȟȤ-ȳɃɆ-ɏḀ-ẞƀ-ƓƗ-ƚƝ-ơƤ-ƥƫ-ưƲ-ƶẠ-ỿ]/, 'gi');


/**
 * Helper. Tests if a string contains diacritics
 * @param {string} string 
 * @returns {boolean}
 */
Extensions.ValidateTitleFieldCommand.prototype._hasDiacritics = function ValidateTitleFieldCommand$_hasDiacritics(string) {
    return this._diacriticRegExp.test(string);
}


/**
 * Helper. Finds diacritics in a string
 * @param {string} string
 * @returns {Array}
 */
Extensions.ValidateTitleFieldCommand.prototype._findDiacritics = function ValidateTitleFieldCommand$_findDiacritics(string) {
    return string.match(this._diacriticRegExp);
}


/**
 * Helper. Removes diacritics from string
 * @param {string} string
 * @returns {string}
 */
Extensions.ValidateTitleFieldCommand.prototype._normalizeString = function ValidateTitleFieldCommand$_normalizeString(string) {
    return string.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}


/**
 * Extracts the title from a Component
 * @param {object} item  and item retrieved with $display.getItem()
 * @returns {string|undefined} if the item is a component, it returns the title
 */
Extensions.ValidateTitleFieldCommand.prototype.getComponentTitle = function ValidateTitleFieldCommand$GetComponentTitle(item) {
    var title;
    if (item) {
        if (item.getItemTypeName() === "Component") {
            title = item.properties.title;
        }
    }

    return title;
}


// Don't Delete. GUI Extension has to come available
Extensions.ValidateTitleFieldCommand.prototype._isAvailable = function ValidateTitleFieldCommand$_isAvailable(selection) {
    return $cme.getCommand("Save")._isAvailable(selection);
};

// Don't Delete. GUI extension must be enabled
Extensions.ValidateTitleFieldCommand.prototype._isEnabled = function ValidateTitleFieldCommand$_isEnabled(selection) {

    return $cme.getCommand("Save")._isEnabled(selection);
};

// Save Command
Extensions.ValidateTitleFieldCommand.prototype._execute = function ValidateTitleFieldCommand$_execute(selection, pipeline) {
    var title = this.getComponentTitle($display.getItem());
    var hasDiacritics = title && this._hasDiacritics(title);

    if (hasDiacritics) {
        // don't try to abstract to another function. The message won't display. 
        var diacriticMatches = this._findDiacritics(title);
        var stringifiedMatches = diacriticMatches.join(',');
        var normalizedTitle = this._normalizeString(title);

        Tridion.MessageCenter.registerError(
            'Invalid Component Title',
            'The title / name of the component contains diacritics. Remove ' + stringifiedMatches + ' and try saving again.',
            'The characters ' + stringifiedMatches + ' are not allowed. \nTry changing the title to ' + normalizedTitle
        );
    } else {
        return $cme.getCommand("Save")._execute(selection, pipeline);
    }
};